const firebase = require('firebase');
const express = require('express');

const VERSION = '1.0';
const HACKER_NEWS = 'https://hacker-news.firebaseio.com';

let ITEMS = [];

//Initialize Firebase
const hn = firebase.initializeApp({ databaseURL: HACKER_NEWS });

//Helper methods
const getPost = (itemNum) => {
	return (new Promise(
		(resolve, reject) => {
			hn.database().ref(`v0/item/${itemNum}`)
				.once('value')
				.then(data => { resolve(data.toJSON()); })
				.catch(error => { reject(error); })
		}
	))
};
const projectPost = (item) => {
	return ({
		id: item.id,
		title: item.title,
		by: item.by,
		url: item.url
	});
};

const loadPost = () => {
	return (new Promise((resolve, reject) => {
		hn.database().ref('v0/newstories')
			.once('value')
			.then(data => {
				ITEMS = data.val();
				resolve();
			})
			.catch(error => {
				reject(error);
			});
	}));
}

//Create an instance of Express 
const app = express();

app.get('/topstories', (req, resp) => {
	const items = [];
	const limit = parseInt(req.query.limit) || 10;
	const offset = parseInt(req.query.offset) || 0;

	const max = ((limit + offset)) > ITEMS.length? ITEMS.length: (limit + offset);

	for (let i = offset; i < max; i++)
		items.push(getPost(ITEMS[i]));

	Promise.all(items)
		.then(result => { 
			resp.status(200).json(result.map(projectPost)); 
		})
		.catch(error => { 
			resp.status(400).json({error: error }); 
		});
});

app.get('/maxitems', (req, resp) => {
	resp.status(200).json({ maxItems: ITEMS.length });
});

app.get('/reload', (req, resp) => {
	loadPost()
		.then(() => { 
			resp.status(200).json({ status: 'reloaded' }); 
		})
		.catch(error => {
			resp.status(400).json({error: error }); 
		});
});

//Use random to push a notification to clients
app.get('/random', (req, resp) => {
	const idx = Math.floor(Math.random() * ITEMS.length);
	getPost(ITEMS[idx])
		.then(projectPost)
		.then(post => { 
			//TODO: Perform the push her

			//End of push
			resp.status(200).json(post);
		})
		.catch(error => { 
			resp.status(400).json({error: error }); 
		});
});

const PORT = process.argv[2] || process.env.APP_PORT || 3000;

loadPost()
	.then(() => {
		console.log('Populated Hacker News: ', ITEMS.length);
		app.listen(PORT, () => {
			console.log(`HNNA ver ${VERSION} started on port ${PORT} at ${new Date()}`);
		});
	})
	.catch(error => {
		console.error('Cannot load Hacker News: ', error);
		process.exit(-1);
	});


